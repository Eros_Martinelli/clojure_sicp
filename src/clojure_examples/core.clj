
(ns clojure-examples.core)



(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))


;; Esercizio 1.3
;; Dati 3 numeri, scrivere una funzione che ritorni la somma dei quadrati dei due maggiori.

(defn square-sum [x y]
  (+ (square x) (square y)))

(defn square [x]
  (* x x))

(defn fun [a b c]
  (square-sum (first (reverse (sort [a b c]))) (second (reverse (sort [a b c])))))


;; Esercizio 1.5
;; Il modello applicative, dovendo valutare la funzione ricorsiva mal-definita p, non converge; mentre il modello normal
;; converge in quanto la valutazione è rimandata alla fine (e p non viene valutata per come è definito il condizionale).
;;
;; Esercizio 1.6
;; Il programma non termina in quanto  la chiamata ricorsiva viene fatta in ogni caso. Questo perché new-if non ignora il resto del
;; codice, anche nel caso in cui il predicato su cui si basa sia soddisfatto.
;;
;; Esercizio 1.7
;; Troppo lungo da riscrivere, guardatelo sul libro.

(defn sqrt [x]
  (sqrt-iter 1.0  x))

(defn sqrt-iter [guess x]
  (if (or (good-guess guess x) (check-guess guess x))
    guess
    (sqrt-iter (improve guess x) x)))


;; Funzioni ausiliarie.

(defn check-guess [guess x]
  (< (abs (- (improve guess x) guess)) 0.0001))


(defn good-guess [guess x]
  ( < (abs (- (square guess) x)) 0.01))

(defn abs [n]
  (if (< n 0) (- n)  n))

(defn square [x]
  (* x x))

(defn average [x y]
  (/ (+ x y) 2))

(defn improve [guess x]
  (average guess (/ x guess)))

;; Esercizio 1.12
;; Implementare una funzione che calcoli il (k,r)-esimo numero di Pascal. 

(defn pascal [i j]
  (cond (= i 0) 1
        (= i j) 1
        :else ( + (pascal (- i 1) (- j 1)) (pascal i (- j 1)))))

;; Esercizio 1.16
;; Scrivere una funzione che calcoli l'esponenziale di un numero in maniera iterativa e che sia logaritmica nei passi compiuti.

(defn exp-iter [a b n]
  (cond (= n 0) a
        (even? n) (exp-iter a (square b) (/ n 2))
        (odd? n) (exp-iter (* a b) b (- n 1))))

(defn exp [b n]
  (exp-iter 1 b n))

;; Esercizio 1.13

(defn double [n]
  (+ n n))

(defn halve [n]
  (/ n 2))

(defn prod [a b]
  (cond (= b 0) 0
        (even? b) (double (prod a (halve b)))
        (odd? b) (+ a (prod a (- b 1)))))

(prod 3 5)

;; Esercizio 1.14
;; Stessa cosa di prima, ma in maniera iterativa. Stavolta l'invariante è i + a*b, dove i sarà il mio risultato finale.

(defn prod-iter [a b] 
  (iter 0 a b))

(defn iter [i a b]
  (cond (= b 0) i
        (even? b) (iter i (double a) (halve b))
        (odd? b)  (iter (+ a i) a (- b 1))))

(prod-iter 3 7)

;; Esercizio 1.15
;; Un altro modo è di esprimere la trasformazione lineare T in forma matriciale rispetto alla base canonica di R^2 e di
;; diagonalizzare la matrice corrispondente. La matrice in forma diagonale, sia essa D, ha come valori gli autovalori (1+- \sqrt{5})/2.
;; Per calcolare il valore dell'n-esimo numero di fibonacci basta calcolare P D^(n) P^(-1)(1,0)^T. In sonstaza si tratta di applicare
;; il fast-mult all'autovalore per poter calcolare la potenza.
;;
;; Se invece si volesse fare come nel libro - dopo un semplice ma noioso calcolo - si arriva alla relazione
;; (T_{p,q})^2 = T_{(p^2 + q^2),(2pq + q^2)}.

(defn square-sum [x y]
  (+ (square x) (square y)))

(square-sum 2 3)

(defn fibonacci-iter [a b p q count]
  (cond (= count 0) b
        (even? count) (fibonacci-iter a b (square-sum p q) (+ (* 2 p q) (square q)) (/ count 2))
        :else (fibonacci-iter (+ (* b q) (* a q) (* a p)) (+ (* b p) (* a q)) p q (- count 1))))

(defn fibonacci [n]
  (fibonacci-iter 1 0 0 1 n))

(defn ratio [n]
  (/ (fibonacci n) (fibonacci (- n 1))))

(fibonacci 60)

;; Esercizio 1.34
;; Sbrogliando (f f) ci si ritrova con (2 2); al che il compilatore dovrebbe dare di matto.

;; Esercizio 1.35
;; Esprimere la golden-ratio come punto fisso di una funzione.

;; Prima di tutto traduciamo in Clojure il codice per calcolare il punto fisso di un'applicazione usato nel libro.

(def tolerance 0.00001)

(defn abs [n]
  (if (< n 0) (- n)  n))


(defn close-enough? [v1 v2]
  (< (abs (- v1 v2)) tolerance))

(defn fixed-point [f first-guess]
  (defn iterator [appo]
    (def guess (f appo))
    (if (close-enough? appo guess)
     guess
     (iterator guess)))
   (iterator first-guess))

(fixed-point #(+ 1 (/ 1 %)) 1)

;; Esercizio 1.40
;; Definire una funzione che restituisca un polinomio monico di terzo grado, in funzione dei coefficienti inseriti.
;; Incomincio con lo scrivere tutte le funzioni di supporto definite nel testo.
      
(def dx 0.00001)

(defn derive [f]
  (fn [x] (/ (- (f (+ x dx)) (f x)) dx)))

(defn newton-transform [f]
  (fn [x] (- x (/ (f x) ((derive f) x)))))

(defn newton-method [f guess]
  (fixed-point (newton-transform f) guess))

(defn cubic [a b c]
  (fn [x] (+ (* x x x)
             (* a x x)
             (* b x) c)))

;; Esercizio 1.42
;; Definire un operatore che componga due funzioni

(defn compose [f g]
  (fn [x] (f (g x))))

;; Esercizio 1.43
;; Creare una funzione che permetta di iterare n-volte l'applicazione di una funzione f.
;; Sulla falsariga di quanto fatto per gli esponenziali, ho scritto due funzioni, una "elementare" ed una "ottimizzata".

(defn repeated-slow [n f]
  (fn [x]
    (if (= n 0) x
        ((compose f (repeated-slow (- n 1) f)) x))))

(defn dbl [x] (* 2 x))

((repeated-slow 2 dbl) 2)

(defn compose-iter [g f n]
  (fn [x]
    (cond (= n 0) (g x)
          (even? n)((compose-iter g (compose f f) (/ n 2)) x)
          (odd? n) ((compose-iter (compose g f) f (- n 1)) x))))

(defn repeated-fast [n f]
  (compose-iter (fn [x] x) f n))

((repeated-fast 2 dbl) 2)

;; Esercizio 2.6
;; Implementare i Church Numerals

(def zero (fn [f] (fn [x] x)))

(defn add-1 [n]
  (fn [f] (fn [x] (f ((n f) x)))))

;; Valutando (add-in zero) trovo

(defn add [x] (+ x 1))

(def one (fn [f] (fn [x] (f x))))

(def two (fn [f] (fn [x] ((compose f f) x))))

;; L'ennesimo numerale di Church, utilizzando la mia funzioncina per calcolare f^n

(def n (fn [f] (fn [x] ((repeated-fast n f) x))))

((zero add) 0)
((one add) 0)

;; Esercizio 2.18
;; Definire una funzione che restituisca l'ultimo elelento di una lista.

(defn last-el [x]
  (if (empty? (rest x)) x (last-el (rest x))))

;; Esercizio 2.19
;; Definire una funzione che abbia come argomento una lista e che restituisca la lista in ordine inverso.

(defn rev [x]
  (if (empty? x) nil (concat (rev (rest x)) (list (first x)))))

(def xs (range 20))
(rev xs)

;; Esercizio 2.21

(map (fn [x] (* x x)) xs)

;; Due implementazioni della funzione per calcolare il numero di foglie di un albero descritta nel libro.

(defn count-leaves [coll]
  (cond
    (not (seq? coll)) 1
    (empty? coll) 0
    :else (+ (count-leaves (first coll)) (count-leaves (rest coll)))))

(defn count-leaves2 [coll]
  (if (seq? coll)
    (apply + (map count-leaves2 coll))
    1))

(def x (list (list 1 2 (list 5 6)) (list 3 4)))

(reverse x)


(count-leaves2 x)

;; Esercizio 2.27
;; Modificare la funzione dell'esercizio 2.19 in modo che restituisca l'inverso di un albero. 

;; Prima soluzione a cui sono giunto io

(defn rev-deep [coll]
  (let [ a  (first (reverse coll))
        b (rest (reverse coll))]
   (cond (empty? coll) nil
        (seq? a) (concat (list (rev-deep a)) (rev-deep b))
        :else (concat (list a) (rev-deep b)))))


(rev-deep x)

;; Soluzione migliorata (trovata in rete)

(defn reve [coll]
  (cond (seq? coll) (reverse (map reve coll))
        :else coll))

(reve x)

;; Esercizio 2.28
;; Scrivere una funzione che restituisca una lista ordinata delle foglie di un albero.

;; Usando flatten è banale

(defn fre [coll]
  (sort (flatten coll)))

;; Posso usare una funzione simile a quella che ho scritto prima. In questo caso "free" libera dalle parentesi.

(defn free [coll]
  (let [a (first coll)
        b (rest  coll)]
   (cond (empty? coll) nil
         (seq? a) (concat (free a) (free b))
         :else (concat  (list a) (free b)))))

(defn fringe [coll]
  (sort (free coll)))

(fringe x)

;; Esercizio 2.29
;; Fare cose sugli alberi.

(defn make-mobile [left right]
  (list left right))

(defn make-branch [lunghezza structure]
  (list lunghezza structure))

(defn left-branch [mobile]
  (first mobile))

(defn right-branch [mobile]
  (first (rest mobile)))

(defn branch-length [branch]
  (first branch))

(defn branch-structure [branch]
  (first (rest branch)))


(defn total-weight [mobile]
  (if (not (seq? mobile)) mobile
      (+ (total-weight (branch-structure (left-branch mobile)))
         (total-weight (branch-structure (right-branch mobile))))))


(defn torque [mobile]
  (- ( * (total-weight (branch-structure (left-branch mobile)))
      (branch-length (left-branch mobile)))
     (* (total-weight (branch-structure (right-branch mobile)))
        (branch-length (right-branch mobile)))))

(defn is-balanced? [mobile]
  (if (not(seq? mobile)) true
      (and (= (torque mobile) 0)
           (is-balanced? (branch-structure (left-branch  mobile)))
           (is-balanced? (branch-structure (right-branch mobile))))))

(def test1 (make-mobile (make-branch 10 4)(make-branch 4 10)))

(def test2 (make-mobile (make-branch 3 10) (make-branch 3 9)))

(def test3 (make-mobile (make-branch 10 1) (make-branch 2 (make-mobile (make-branch 4 1) (make-branch 1 4)))))


(is-balanced? test1)
(is-balanced? test2)
(is-balanced? test3)

;; Esercizio 2.30-2.31

(defn tree-map [tree f]
  (map (fn [sub-tree]
         (if (seq? sub-tree)
           (tree-map sub-tree f)
           (f sub-tree)))
       tree))

(defn square [x]
  (* x x))

(defn square-tree [tree]
  (tree-map tree square))

(def tree  (list (list 1 2 (list 7 6)) (list 3 4)))

(square-tree tree)

;; Esercizio 2.32
;; Scrivere una funzione che restituisca la lista delle sottoliste di una data lista.

(defn subsets [l]
  (if (empty? l) (list (list))
      (concat (subsets (rest l))
              (map (fn [el] (conj el (first l))) (subsets (rest l))))))


(def l (range 1 4))

(def sub (list (list ) (list 3 ) (list 2 ) (list 2 3 ) (list 1) (list 1 3) (list 1 2) (list 1 2 3)))

( = (subsets l) sub)

;; Esercizio 2.33
;; Esprimere map, append, e la lunghezza di una lista in termini di reduce.

(defn mapre [f xs]
  (reduce (fn [x y] (concat x (list (f y)))) nil xs))

(defn conc [x1 x2]
  (reverse (reduce conj (reverse x1) x2)))

;; Nota: qui avrei voluto usare cons (come consigliato dal libro), ma putroppo (cons lista el) in clojure mi da errore).

(defn lunghezza [xs]
  (reduce (fn [counter el] (+ counter 1)) 0 xs))

;; Esercizio 2.34

(defn horner-eval [x xs]
  (reduce (fn [terms coef] (+ (* terms x ) coef)) 0 xs))

(def pol  (reverse(list 1 3 0 5 0 1)))

(horner-eval 2 pol)

;; Esercizio 2.38
;; Qui errore mio, credevo la funzione definita nel libro fosse un fold-left, mentre in realtà è un fold-right. Da qui l'utilizzo dei
;; reverse negli esercizi precedenti. Ovviamente, ordine a parte, le due danno lo stesso risultato (per tutti gli imput) sse
;; l'operazione a cui vengono applicate è associativa.
;; Visto l'errore, ripropongo gli esercizi che avevo già fatto prima di accorgermene, utilizzando il fold-right del libro.

(defn fold-right [op initial xs]
  (if (empty? xs) initial
      (op (first xs) (fold-right op initial (rest xs)))))

;; Esercizio 2.33 (Rifatto con accumulate come da libro)

(defn mapre-right [f xs]
  (fold-right (fn [x y] (concat (list (f x)) y )) nil xs))

(defn conc-right [x1 x2]
  (reverse (fold-right cons (reverse x1) (reverse x2))))

(conc-right (range 2) (range 3 5))

(defn lunghezza-right [xs]
  (fold-right (fn [x y] (+ y 1)) 0  xs))

(lunghezza-right (range 30))

;; Esercizio 2.34

(defn horner-eval-right [x xs]
  (fold-right (fn [coef terms] (+ (* terms x ) coef)) 0 xs))

(def pol (list 1 3 0 5 0 1))

(horner-eval-right 2 pol)

;; Esercizio 2.35 (abbastanza difficile, soprattutto perchè avevo già in mente un'altra soluzione).
;; Implementare una funzione per calcolare il numero di foglie di un albero dato, usando fold-right e map.
;; L'idea piü ovvia è di usare enumerate e lunghezza-right per ottenere il numero di foglie, solo che l'esercizio richiede
;; "riempire" uno "scheletro" dato. La soluzione è quella di andare in profondità e calcolare la lunghezza delle sottoliste in
;; maniera ricorsiva, interpretando i singleton come liste composte da un solo elemento. Fatto questo basta sommare la lista per
;; trovare il numero di foglie. 

;; Le due implementazioni citate.

(defn count-leaves [coll]
  (cond
    (not (seq? coll)) 1
    (empty? coll) 0
    :else (+ (count-leaves (first coll)) (count-leaves (rest coll)))))

(defn count-leaves2 [coll]
  (if (seq? coll)
    (apply + (map count-leaves2 coll))
    1))

;; La soluzione che mi è venuta subito in mente

(defn count-fake [coll]
  (lunghezza-right (flatten coll)))

(defn count-leaves-right [coll]
  (fold-right (fn [x y] (+ x y)) 0 (map (fn [el] (cond (nil? el) 0
                                                       (not (seq? el)) 1
                                                       (seq? el) (count-leaves-right el))) coll)))

(def tester (list (list 1 2 (list 5 6)) (list 3 4)))

(= (count-fake tester) (count-leaves-right tester))

;; Esercizio 2.36

(defn accumulate-n [f init seqs]
  (if (empty? (first seqs))
    ()
    (cons (fold-right f init (map (fn [xs] (first xs)) seqs))
          (accumulate-n f init (map (fn [xs] (rest xs)) seqs)))))


(def prova (list (list 2 3) (list 3 4) (list 5 6)))

(accumulate-n (fn [x y] (+ x y)) 0 prova)

;; Esercizio 2.40
;; In questo caso semplifico le cose utilizzando la list comprehension.

(defn unique-pairs [n]
  (for [i (range 1 (+ 1 n))
        j  (range 1 (+ 1 n)) :when (< j i)] (list i j)))

;; Visto che ho saltato la parte in cui si facevano i crivelli, ne scrivo uno io on the fly.

(defn prime? [n]
  ( = (filter (fn [x] (= (rem n x) 0)) (range 1 (+ n 1))) (list 1 n)))


(defn prime-sum-pairs [n]
  (filter (fn [xs] (prime? (last xs)))
          (map (fn [el] (list (first el) (last el) (+ (first el) (last el))))
               (unique-pairs n))))

(prime-sum-pairs 7)

;; Esercizio 2.41

(defn ord-trip-sum [n s]
  (for [i (range  (+ 1 n))
        j (range  (+ 1 n))
        k (range  (+ 1 n))
        :when (and (< i j k) (= (+ i j k) s))] (list i j k)))

;; Esercizio 2.42
;; Il problema delle 8 regine. Onestamente ho trovato il codice scritto nel libro un pochettino pesante da leggere, specialmente
;; la parte relativa al flatmap; per semplificare le cose ho deciso di tradurre quellla condizione in un linguaggio a me piu'
;; comprensibile.
;; Come prima cosa, per semplificare, poniamo
;; Queen-cols(n-1) := (queen-cols (- n 1)), aggiungi((new-row, k), rest-of-queens):=(adjoin-position new-row k rest-of-queens).
;; Notiamo che (enumerate-interval 1 board-size) è equivalente a (range (1 (+ 1 board-size))) e per semplificare poniamo
;; range(board-size) := (range (1 (+ 1 board-size))).
;; In questo modo abbiamo che la soluzione puo' essere espressa come:
;; \forall sol \in Queen-cols(n-1), \forall j \in range(board-size): aggiungi((j,k), sol) if safe(j, pos), dove safe(j,pos) è il
;; predicato che esprime l'ammissibilità della regina posizionata in (j,k) rispetto a quelle contenute in sol.


(def empty-board (list)) ;; Piü che la board vuota, questo rappresenta la soluzione vuota.

(defn adjoin-pos [k j sol]  ;; Nota che l'ordine è invertito, il primo elemento è quello relativo all'ultima regina aggiunta.
  (cons (list k j) sol))

(defn relative-safe? [x1 y1 x2 y2]          ;; Un semplice calcolo mostra che sulle diagonali si preservano la diff. e la somma delle 
  (and (not= y1 y2)                         ;; coordinate.
       (not= (- x1 y1)  (- x2 y2))
       (not= (+ x1 y1) (+ x2 y2))))

(defn safe? [k j sol]
  (reduce (fn [x y] (and x y)) true (map (fn [el] (relative-safe? k j (first el) (last el))) sol)))

(defn queens [board-size]
  (defn queen-cols [k]
    (if (= k 0)
      (list empty-board)
      (for [sol (queen-cols (- k 1))
            j (range 1 (+ 1 board-size)) :when (safe? k j sol)] (adjoin-pos k j sol))))
  (queen-cols board-size))
          
(= 92 (count (queens 8)))
(= 10 (count (queens 5)))
(= 0 (count (queens 2)))

;; Esercizio 2.56
;; Estendere la funzione deriv implementando la regola esponenziale.

(defn deriv [exp var]
  (cond (number? exp) 0
        (variable? exp) (if (same-variable? exp var) 1 0)
        (sum? exp) (make-sum (deriv (addend exp) var)
                             (deriv (augend exp) var))
        (product? exp) (make-sum (make-product (multiplier exp)
                                               (deriv (multiplicand exp) var))
                                 (make-product (deriv (multiplier exp) var)
                                               (multiplicand exp)))
        (exponential? exp) (make-product (make-product (exponent exp)
                                                       (make-exponential (base exp) ( - (exponent exp) 1)))
                                         (deriv (base exp) var))
        :else (throw (Exception. "unknown expression type."))))

(defn variable? [x] (symbol? x))

(defn same-variable? [v1 v2]
  (and (variable? v1) (variable? v2) (= v1 v2)))

(defn make-sum [a1 a2] (list '+ a1 a2))

(defn make-product [m1 m2] (list '* m1 m2))

(defn sum? [x] (and (seq? x) (= (first x) '+)))

(defn addend [s] (second s))

(defn augend [s] (second (rest s)))

(defn product? [x] (and (seq? x) (= (first x) '*)))

(defn multiplier [p] (second p))

(defn multiplicand [p] (second (first p)))

(defn exponential? [x] (and (seq? x) (= (first x) '**)))

(defn make-exponential [base expo] (list '** base expo))

(defn base [p] (second p))

(defn exponent [p] (second (rest p)))

(def prova (make-exponential 'x 2))

(deriv prova 'x)

;; Esercizio 2.57

(defn augend2 [s] (reduce op (rest (rest s))))

(defn multiplicand2 [p] (reduce op2 (rest (rest p))))

(defn op2 [x y] (list '* x y))

(defn op [x y] (list '+ x y))

(defn deriv2 [exp var]
  (cond (number? exp) 0
        (variable? exp) (if (same-variable? exp var) 1 0)
        (sum? exp) (make-sum (deriv2 (addend exp) var)
                             (deriv2 (augend2 exp) var))
        (product? exp) (make-sum (make-product (multiplier exp)
                                               (deriv2 (multiplicand2 exp) var))
                                 (make-product (deriv2 (multiplier exp) var)
                                               (multiplicand2 exp)))
        :else (throw (Exception. "unknown expression type."))))

(deriv2 '(* x y (+ x 3)) 'x)

;; Esercizio 2.58
;; La prima parte è immediata, basta modificare leggermente i selettori, i costruttori e i vari predicati.

(defn make-product-prefix [v1 v2] (list v1 '* v2))

(defn make-sum-prefix [v1 v2] (list v1 '+ v2))

(defn product-prefix? [exp] (and (seq? exp) (= (second exp) '*)))

(defn sum-prefix? [exp] (and (seq? exp) (= (second exp) '+)))

(defn addend-prefix [s] (first s))

(defn augend-prefix [s] (last s))

(defn multiplier-prefix [p] (first p))

(defn multiplicand-prefix [p] (last p))

(defn deriv-prefix [exp var]
  (cond (number? exp) 0
        (variable? exp) (if (same-variable? exp var) 1 0)
        (sum-prefix? exp) (make-sum-prefix (deriv-prefix (addend-prefix exp) var)
                             (deriv-prefix (augend-prefix exp) var))
        (product-prefix? exp) (make-sum-prefix (make-product-prefix (multiplier-prefix exp)
                                               (deriv-prefix (multiplicand-prefix exp) var))
                                 (make-product-prefix (deriv-prefix (multiplier-prefix exp) var)
                                               (multiplicand-prefix exp)))
        :else (throw (Exception. "unknown expression type."))))

(deriv-prefix '(x + (3 * (x + (y + 2)))) 'x)

;; La seconda parte mi ha fatto soffrire parecchio; la mia idea era di implementare una funzione che permettesse di aggiungere
;; le parentesi "mancanti" in modo da potermi ricondurre al problema precedente. L'idea era di implementare questa funzione in maniera
;; ricorsiva. Sia f la funzione che descrive questa procedura, allora possiamo definire f in maniera ricosiva utilizzando le seguenti
;; relazioni (dove x puo' tranquillamente essere un'espressione annidata):
;; -- f( (x + tail)) = (f(x) + (f(tail)));
;; -- f(x * y op tail) = ((f(x) * f(y)) op (f(tail))).

(def prova3 '((x * (j + k * t + w) + w + (y * z * w))))

(defn converter [exp]
 (cond (not (seq? exp)) exp
       (= 1 (count exp )) (converter (first exp))
       :else (if (= (second exp) '*)
               (*-case exp converter)
               (+-case exp  converter))))

(defn +-case [exp, f]
  (concat (list(f (first exp))
               (second exp))
          (f  (rest (rest exp)))))

(defn *-case [exp, f]
  (concat (list (list (f (first exp)) (second exp) (f (second (rest exp)))))
          (continue (rest (rest exp)) f)))

(defn continue [exp f]
  (if (nil? (second exp)) ()
      (list (second exp) (f (rest (rest  exp))))))
          
(converter prova3)

(deriv-prefix (converter prova3) 'x)

;; Esercizio 2.59
;; Implementare l'unione di due insiemi (visti come liste non ordinate).

;; Incominciamo con le varie operazioni implementate nel libro.

(defn element-of-set? [x insieme]
  (cond (empty? insieme) false
        (= x (first insieme)) true
        :else (element-of-set? x (rest insieme))))

(defn adjoin-set [x insieme]
  (if (element-of-set? x insieme)
    insieme
    (cons x insieme)))


(defn intersection-set [set1 set2]
  (cond (or (empty? set1) (empty? set2)) ()
        (element-of-set? (first set1) set2)
        (cons (first set1) (intersection-set (rest set1) set2))
        :else (intersection-set (rest set1) set2)))

;; Definiamo l'unione aggiungendo ogni elemento di s1 che non sia già in s2

(defn union-set [set1 set2]
  (reduce (fn [s el] (adjoin-set el s)) set2 (reverse set1)))

(def s1  (range 6))

(def s2 (range 4 8))

(element-of-set? 3 s1)

(element-of-set? 7 s1)

(adjoin-set 7 s1)

(intersection-set s1 s2)

(union-set s1 s2)

;; Esercizio 2.60

;; Il predicato di appartenenza non cambia.

;; Aggiungere un elemento ad un insieme diventa semplicemente prependerlo senza controllare che ci sia già, diventando un O(1) invece
;; che O(n).

(defn adjoin-set-rep [x insieme] (cons x insieme))

;; L'intersezione, essendo definita in termini del predicato di appartenenza non cambia.

;; L'unione, non dovendo controllare le ripetizioni, diventa una semplice concatenazione, trasformandosi in un O(n).

(def s3 '( 1 2 1 3 1))

(def s4 '(1 1 3 3 5))

(intersection-set s3 s4)

;; In conclusione, quest'ultima scelta è piü vantaggiosa a livello computazionale (a meno che non si voglia calcolare la cardinalità
;; di un insieme) ma - personalmente - la trovo visualmente orrenda e poco chiara. Quello che farei io sarebbe di utilizzare questa
;; rappresentazione per le varie operazioni e qualora necessitassi di stampare a schermo un insieme, implementare una funzione che
;; elimini i doppioni.

;; Esercizio 2.61

(defn element-of-set-ord? [x s1]
  (cond (empty? s1) false
        (= x (first s1)) true
        (< x (first s1)) false
        :else (element-of-set-ord? x (rest s1))))

(defn intersection-set-ord [set1 set2]
  (if (or (empty? set1) (empty? set2)) ()
      (let [x1 (first set1) x2 (first set2)]
        (cond (= x1 x2)
              (cons x1 (intersection-set-ord (rest set1) (rest set2)))
              (< x1 x2)
              (intersection-set-ord (rest set1) set2)
              (< x2 x1)
              (intersection-set-ord set1 (rest set2))))))

(intersection-set-ord s1 s2)

(defn adjoin-set-ord [el s1]
  (let [x1 (first s1)]
    (cond (empty? s1) (list el)
          (< el x1) (cons el s1)
          (= el x1) s1
          :else (cons x1 (adjoin-set-ord el (rest s1))))))

(adjoin-set-ord 5 s2)

;; Esercizio 2.62
;; Potrei fare come fatto prima, ma in tal caso otterrei una funzione con complessità O(n^2) e non O(n) come richiesto. Per ottenere
;; la complessità richiesta, mimo quanto fatto per l'intersezione.

(defn union-set-ord [set1 set2]
  (let [x1 (first set1) x2 (first set2)]
    (cond (empty? set1) set2
          (empty? set2) set1
          (< x1 x2) (cons x1 (union-set-ord (rest set1) set2))
          (< x2 x1) (cons x2 (union-set-ord set1 (rest set2)))
          :else (cons x1 (union-set-ord (rest set1) (rest set2))))))

(union-set-ord (list 1 3 4) (list 1 2 5))


(defn entry [tree] (first tree))

(defn left-branch [tree] (second tree))

(defn right-branch [tree] (second (rest tree)))

(defn make-tree [entry left right] (list entry left right)) 

(defn element-of-set-tree? [x set1]
  (cond (empty? set1) false
        (= (entry set1) x) true
        (< x (entry set1)) (element-of-set-tree? x (left-branch set1))
        (> x (entry set1) ) (element-of-set-tree? x (right-branch set1))))


(defn adjoin-set-tree [x set1]
  (cond (empty? set1) (make-tree x () ())
        (= x (entry set1)) set1
        (< x (entry set1))
        (make-tree (entry set1)
                   (adjoin-set-tree x (left-branch set1))
                   (right-branch set1))
        (> x (entry set1))
        (make-tree (entry set1) (left-branch set1)
                   (adjoin-set-tree x (right-branch set1)))))

(def tree '(7 (3 (1 () ()) (5 () ())) (9 () (11 () ())))) 

(element-of-set-tree? 5 tree)

(element-of-set-tree? 2 (adjoin-set-tree 2 tree))

;; Esercizio 2.63

(defn tree->list1 [tree]
  (if (empty? tree) '()
      (concat (tree->list1 (left-branch tree))
              (cons (entry tree)
                    (tree->list1
                     (right-branch tree))))))

;; Il primo crea una lista in cui la prima parte è la funzione chiamata sul lato sinistro, unita al nodo centrale, unita alla funzione
;; chamata sul lato destro; in sostanza è una specie di quick-sort in cui non si devono cercare gli elementi minori o maggiori al
;; pivot (data la struttura dell'albero). 
;; La complessità è data dal caso migliore del quick-sort, cioè O(n*log(n)).
;; Per alberi bilanciati, ogni volta che faccio una chiamata ad un lato dovrei dimezzare la complessità. Contando che faccio questa
;; cosa due volte (destra-sinistra) per ogni chiamata e che appendere liste è lineare, la complessità dovrebbe essere data da
;; C(n) = 2*C(n/2) + O(n).
;; In effetti, sostituendo a C(n) la stima data, a meno di un fattore 1/2 asintoticamente irrilevante, la cosa torna.

(defn tree->list2 [tree]
  (defn copy-to-list [tree result-list]
    (if (empty? tree)
      result-list
      (copy-to-list (left-branch tree)
                    (cons (entry tree)
                          (copy-to-list (right-branch tree)
                                        result-list)))))
  (copy-to-list tree (list )))

;; La seconda ricorda vagamente un fold-right. Ad ogni passo la lista viene divisa in due pezzi e la funzione chiamata ricorsivamente
;; su ciascuno dei due pezzi in questo modo: f(left (x::f(right))). L'analisi di prima dovrebbe ancora valere, con la differenza che
;; ora la complessità della funzione di "appoggio" (append prima e prepend ora) è O(1). Fatto tutto, dovremmo avere una complessità
;; lineare (risolvendo con Wolfram C(n) = 2*C(n/2) + cost - asintoticamente - viene O(n)).

;; Esercizio 2.64

(defn list->tree [elements]
  (defn partial-tree [elts n]
    (if (= n 0)
      (cons '() elts)
      (let [left-size (quot (- n 1) 2)]
        (let [left-result (partial-tree elts left-size)]
          (let [left-tree (first left-result)
                non-left-elts (rest left-result)
                right-size (- n (+ left-size 1))]
            (let [this-entry (first non-left-elts)
                  right-result (partial-tree (rest non-left-elts) right-size)]
              (let [right-tree (first right-result)
                    remaining-elts (rest right-result)]
                (cons (make-tree this-entry
                                 left-tree
                                 right-tree)
                      remaining-elts))))))))
  (first (partial-tree elements (count elements))))

(defn execution-time [exp]
  (with-out-str (time exp)))

;; Sbrogliando i vari let che la funzione presenta, si nota che:
;; - Il primo let restituisce il lato sinistro del nostro wannabe tree tramite una chiamata ricorsiva in vengono presi in
;;   considerazione solo la prima metà degli elementi (ma applicando la funzione a tutta la lista), cioè dimezzandone la lista;
;; - Il secondo let esegue operazioni di complessità costante sul risultato ottenuto nel punto precedente. In particolare, viene
;;   estratta la lista contenente gli elementi rimanenti: non-left-elts;
;; - Il terzo let serve a definire la radice del wannabe albero (il primo elemento di non-left-elts) e a definire il suo lato destro
;;   applicando la funzione alla coda di non-left-elts;
;; - L'ultimo let è simile al secondo ma per il lato destro ottenuto nel punto precedente;
;; - Infine, utilizzando la funzione che crea un albero - che ha complessità costante - vengono uniti assieme i pezzi per formare
;; - l'albero.
;; In conclusione, come prima abbiamo
;; C(n) = 2*C(n/2) + cost,
;; che da una complessità lineare. 

(def prova (list->tree (range 10)))
(def prova2 (list->tree (range 8 12)))

(= (tree->list1 prova) (tree->list2 prova))

;; Esercizio 2.65
;; Basta usare l'unione e l'intersezione per le liste ordinate e le funzioni definite nei due esercizi precedenti.

(defn union-set-tree [set1 set2]
  (list->tree (union-set-ord (tree->list2 set1) (tree->list2 set2))))

(defn intersection-set-tree [set1 set2]
  (list->tree (intersection-set-ord (tree->list2 set1) (tree->list1 set2))))

(union-set-tree prova prova2)

(intersection-set-tree prova prova2)

;; Esercizio 2.66
;; I nodi dell'albero sono coppie in cui il primo elemento è la chiave e il secondo è la lista dei valori associati ad essa.
;; Come prima cosa implemento i costruttori e i metodi necessari; la funzione richiesta è ottenibile con una piccola modifica della
;; funzione "element-of-set-tree?".

(defn make-node [keyval values] (list keyval values))

(defn get-key [record] (first record))

(defn get-values [record] (second record))

(defn lookup-tree [given-key records]
  (cond (empty? records) false
        (= (get-key (entry records)) given-key) true
        (< given-key (get-key (entry records))) (lookup-tree given-key (left-branch records))
        (> given-key (get-key (entry records) )) (lookup-tree given-key (right-branch records))))

;; Esercizio 2.67
;; Tutte le funzioni definite nel libro

(defn make-leaf [symb weight] (list 'leaf symb weight))

(defn leaf? [object] ( = (first object) 'leaf))

(defn symbol-leaf [x] (second x))

(defn weight-leaf [x] (second (rest x)))

(defn left-branch [tree] (first tree))

(defn right-branch [tree] (second tree))

(defn symbols [tree]
  (if (leaf? tree)
    (list (symbol-leaf tree))
    (first (rest (rest tree)))))

(defn weight [tree]
  (if (leaf? tree)
    (weight-leaf tree)
    (second (rest (rest tree)))))

(defn make-code-tree [left right]
  (list left right
        (concat (symbols left) (symbols right))
        (+ (weight left) (weight right))))

(defn decodes [bits tree]
  (defn decode-1 [bits current-branch]
    (if (empty? bits) '()
        (let [next-branch (choose-branch (first bits) current-branch)]
          (if (leaf? next-branch)
            (cons (symbol-leaf next-branch)
                  (decode-1 (rest bits) tree))
            (decode-1 (rest bits) next-branch)))))
  (decode-1 bits tree))

(defn choose-branch [bit branch]
  (cond (= bit 0) (left-branch branch)
        (= bit 1) (right-branch branch)
        :else (throw (Exception. "Inserire solo 0 o 1"))))

(defn adjoin-set-huffman [x tree]
  (cond (empty? tree) (list x)
        (< (weight x) (weight (first tree))) (cons x tree)
        :else (cons (first tree) (adjoin-set-huffman x (rest tree)))))

(defn make-leaf-set [pairs]
  (if (empty? pairs) ()
      (let [pair (first pairs)]
        (adjoin-set-huffman (make-leaf (first pair)
                                       (second pair))
                            (make-leaf-set (rest pairs))))))

;; L'esercizio vero e proprio

(def sample-tree (make-code-tree (make-leaf 'A 4)
                  (make-code-tree
                   (make-leaf 'B 2)
                   (make-code-tree
                    (make-leaf 'D 1)
                    (make-leaf 'C 1)))))

(def sample-message '(0 1 1 0 0 1 0 1 0 1 1 1 0))

(decodes sample-message sample-tree)

;; Esercizio 2.68

(defn encode [message tree]
  (if (empty? message) (list)
      (concat (encode-symbol (first message) tree)
              (encode (rest message) tree))))

(defn encode-symbol [symb tree]
  (cond (leaf? tree) ()
        (left? symb tree) (cons 0 (encode-symbol symb (left-branch tree)))
        (right? symb tree) (cons 1 (encode-symbol symb (right-branch tree)))
        :else (throw (Exception. "Il simbolo inserito non è presente"))))

(defn left? [symb tree]
  (in? (symbols (left-branch tree)) symb))

(defn right? [symb tree]
  (in? (symbols (right-branch tree)) symb))

(defn in? [l el]
  (cond (empty? l) false
        (= (first l) el) true
        :else (in? (rest l) el)))

(= (encode (decodes sample-message sample-tree) sample-tree) sample-message)

;; Esercizio 2.69

(defn generate-huffman-tree [pairs]
  (successive-merge (make-leaf-set pairs)))

(defn successive-merge [lista]
  (cond (empty? (rest lista)) (first lista)
        :else (successive-merge (adjoin-set-huffman (make-code-tree (first lista) (second lista)) (rest (rest lista))))))

;; L'ordine sui pesi non garantisce che la funzione definita precedentemente generi lo stesso albero, in quanto anche la mutua
;; posizione delle coppie aventi lo stesso peso influisce sul risultato finale. 

(def lista (list  (make-leaf 'D 1) (make-leaf 'C 1) (make-leaf 'B 2) (make-leaf 'A 4)))

(def lista2 (list  (make-leaf 'C 1) (make-leaf 'D 1) (make-leaf 'B 2) (make-leaf 'A 4)))

(= (successive-merge lista) (successive-merge lista2)) ;; => false


(def pairs (list (make-leaf 'H 1) (make-leaf 'G 1) (make-leaf 'F 1) (make-leaf 'E 1) (make-leaf 'D 1) (make-leaf 'C 1) (make-leaf 'B 3) (make-leaf 'A 8)))

(def pairs2 (list '(H 1) '(G 1) '(F 1) '(B 3) '(E 1) '(D 1) '(C 1) '(A 8)))

(= (generate-huffman-tree pairs2) (successive-merge pairs)) ;; => false 

;; Esercizio 2.70

(def alphabeth (list '(A 2) '(GET 2) '(SHA 3) '(WAH 1) '(BOOM 1) '(JOB 2) '(NA 16) '(YIP 9)))

(def tree (generate-huffman-tree alphabeth))

(def message-to-be-encoded '(GET A JOB SHA NA NA NA NA NA NA NA NA GET A JOB SHA NA NA NA NA NA NA NA NA WAH YIP YIP YIP YIP YIP YIP YIP YIP YIP SHA BOOM))

(def encoded-message (encode message-to-be-encoded tree))

(count encoded-message) ;; => 84

(count message-to-be-encoded) ;; => 36

;; Con 8 simboli abbiamo bisogno di almeno 3 bits per ciascun simbolo; visto message-to-be-encoded contiene 36 elementi, la lunghezza
;; finale sarebbe di 108, mentre nel nostro caso abbiamo una lunghezza di 84.

(def alph32 (list '(A 1) '(B 2) '(C 4) '(D 16) '(E 32)))

(def tree32 (generate-huffman-tree alph32))

(def least (encode-symbol 'A tree32))

(def most (encode-symbol 'E tree32))

;; L'albero generato con questo tipo di valori è un albero in cui la j-esima foglia viene sempre aggiunta a dx nella costruzione
;; induttiva dell'albero stesso. Per questo motivo il simbolo meno frequente richiede n-1 bits per essere codificato, mentre quello
;; piü frequente solo 1; in particolare, il primo verrà sempre codificato da una lista di n-1 zeri, mentre il secondo da un '1'.

;; Esercizio 2.72
;;
;; La funzione 'encode-symbol'- ad ogni passo - viene chiamata in maniera ricorsiva su uno dei due rami di un nodo per questo
;; abbiamo un fattore logaritmico nella complessità. Ad ogni passo, per determinare quale ramo prendere, viene applicata una funzione
;; che controlla in quale ramo stia il simbolo da critpare; per fare ciö, questa funzione passa in rassegna (potenzialmente tutta) la
;; lista dei caratteri da codificare. Tenendo conto di queste considerazioni, se volessimo calcolare la complessità in funzione dei
;; della lunghezza dell'alfabeto, avremmo che
;; C(n) = C(n/2) + O(n),
;; che da come risultato C(n) = O(n*log(n)).
;;
;; Nel caso in cui l'alfabeto sia come nell'esercizio precedente, per codificare 'A dovremmo percorrere tutto l'albero fino in
;; fondo (eseguendo ad ogni passo una ricerca sul ramo da seguire, ricerca che ha complessità lineare), per un totale di n*(n-1)
;; passi, mentre per codificare 'E ci bastano n passi (bisogna solo capire se andare a destra o a sinistra). Per concludere abbiamo:
;; - Codifica simbolo con minore frequenza => O(n^2);
;; - Codifica simbolo con maggiore frequenza => O(n).


(defn make-account [balance]
  (defn withdraw [amount]
    (if (>= @balance amount)
      (reset! balance (- @balance amount))
      "Fondi Insufficienti"))
  
  (defn deposit [amount]
    (reset! balance (+ @balance amount)))
  
  (defn dispatch [reference]
    (cond (= 'withdraw reference) withdraw
          (= 'deposit reference) deposit
          :else "Inserire un'operazione valida"))
  dispatch)


(def bilancio (atom 100))

(def Account (make-account bilancio))

((Account 'withdraw) 10)

((Account 'deposit) 10)

;; Esercizio 3.1

(defn make-accumulator [value]
  (fn [sum] (reset! value (+ @value sum))))

(def A (make-accumulator (atom 10)))

(A 10)

;; Esercizio 3.2
;; All'inizio credevo di dover contare il numero delle eventuali chiamate ricorsive della funzione impunt e non avevo idea di come
;; fare; fortuna che poi ho capito che quello da contare erano soli il numero di volte in cui applicavo la funzione. 

(defn make-monitored [function]
  (def times (atom 0))
  
  (defn apply-count [f argument]
    (list (reset! times (+ @times 1)) (f argument)))
  
  (defn dispatch [option]
    (cond (= 'how-many-calls? option) @times
          :else (second (apply-count f option))))
  
  dispatch)

(defn f [a] (+ a 1))

(def A (make-monitored f))

(A 10)

(A 'how-many-calls?)

;; Esercizio 3.3 + 3.4

(defn make-account-password [balance password]

  (def counter (atom 0))

  (defn reset-counter []
    (reset! counter 0))

  (defn call-police []
    (str "La polizia è stata chiamata"))

  (defn incorrect-password []
    (if (< @counter 7)
      (do (reset! counter (+ @counter 1))
          (str "Password non valida"))
      (call-police )))

  (defn withdraw [amount]
    (if (>= @balance amount)
      (reset! balance (- @balance amount))
      "Fondi Insufficienti"))
  
  (defn deposit [amount]
    (reset! balance (+ @balance amount)))
  
  (defn dispatch [word reference]
    (cond (and (= word password) (= 'withdraw reference)) (do (reset-counter) withdraw)
          (and (= word password) (= 'deposit reference)) (do (reset-counter) deposit)
          :else (if (= word reference) "Inserire un'operazione valida"
                    (incorrect-password  ))))
  dispatch)

(def B (make-account-password (atom 100) 'prova))

((B 'prova 'deposit) 10)

(B 'prov 'deposit)

((B 'prova 'withdraw) 10)

;; Esercizio 3.7
;;
;; Non sono molto soddisfatto di questa soluzione. 

(defn make-account-shared [balance password]

  (defn withdraw [amount]
    (if (>= @balance amount)
      (reset! balance (- @balance amount))
      "Fondi Insufficienti"))
  
  (defn deposit [amount]
    (reset! balance (+ @balance amount)))
  
  (defn dispatch [word reference]
    (cond (and (= word password) (= 'withdraw reference))  withdraw
          (and (= word password) (= 'deposit reference))  deposit
          (and (= word password) (= reference 'password)) true 
          :else (if (= word reference) "Inserire un'operazione valida"
                     "Password non valida")))
  dispatch)

(defn make-joint [account password new-password]
  (defn make-new [acc pass]
    (defn dispatch [word reference]
    (cond (and (= word new-password) (= 'withdraw reference))  (account password 'withdraw)
          (and (= word new-password) (= 'deposit reference))  (account password 'deposit)
          :else (if (= word reference) "Inserire un'operazione valida"
                     "Password non valida")))
    dispatch)
   (if (account password 'password)
    (make-new account new-password)
    "La password inserita non è valida"))

(def paul-acc (make-account-shared (atom 100) 'prova))

(def paolo (make-joint paul-acc 'prova 'nuova))

((paul-acc 'prova 'deposit) 10)

((paolo 'nuova 'withdraw) 10)

;; Esercizio 3.8

(def init (atom 1))

(defn count [amount]
  (reset! init (* @init amount)))

(+ (count 1) (count 0))

(+ (count 0) (count 1))

;; Esercizio 3.12

;; Provo ad utilizzare le liste linkate di Java per definire le funzioni del libro.

(import java.util.LinkedList)

(defn linked-list
  ([] (java.util.LinkedList. ()))
  ([a] (java.util.LinkedList. a )))

(defn set-car! [el lista]
  (.set lista 0 el))

(defn rest! [xs]
  (.subList xs 1  (.size xs)))

(defn append! [list1 list2]
  (.addAll list1 list2))

(defn set-cdr! [lista1 lista-originale]
  (do (.clear (rest! lista-originale))
      (append! lista-originale lista1)
       lista-originale))


(def a 10)

(def b 15)

(def c 20)



(def pair (list a b))

(def pair2 (list 2 4))

(def ad 10)


;;(defn node-name [node]
;;  (str (deref (first node))))

(defn empty-node []
  (list nil (atom "nil")))


(defn create-node [v1]
  (list v1 (atom (empty-node ))))

(defn link-a-node [node node-to-be-linked]
  (reset! (second node) node-to-be-linked))



(def node (create-node2 a))

a

(def node2 (create-node2 b ))


(def node3 (create-node c))





;;(link-a-node node node2)

;;(deref (second node))

;;(link-a-node node2 node3)

;;(second (deref (second node)))

(defn get-value-node [node]
  (first node))

(defn get-pointer-node [node]
  (deref (second node)))

(defn set-pointer! [node new-node]
  (reset! (second  node) new-node))

(defn emtpy-mutable-list [] (list (atom "head") (atom "tail")))

(defn get-head [xs]
  (if (is-empty? xs) nil
      (get-value-node (deref (first xs)))))

(def xs (emtpy-mutable-list ))

(def prova (emtpy-mutable-list ))


xs

(defn is-empty? [xs]
  (= (deref (first xs)) "head"))

(defn has-tail? [xs]
   (not (= (deref (second xs)) "tail")))

;;(defn add-value [xs elem]
  ;;(if (is-empty? xs) (do (let [a (create-node elem)]
    ;;                       (reset! (first xs) a ))


(def parent {:id 1 :children (atom nil) :parent (atom nil)})

(def child  {:id 2 :children (atom nil) :parent (atom nil)}) 

(swap! (:children parent) conj child)
(reset! (:parent child) parent)

(:id @(:parent child))

(def nodo1 (list 1 (atom nil) (atom nil)))

(def nodo2 (list 2 (atom nil) (atom nil)))

(reset! (second nodo1) nodo2)

(reset! (second (rest nodo2)) nodo1)

(first @(second nodo1))

(def asym-hobbit-body-parts [{:name "head" :size 3}
                             {:name "left-eye" :size 1}
                             {:name "left-ear" :size 1}
                             {:name "mouth" :size 1}
                             {:name "nose" :size 1}
                             {:name "neck" :size 2}
                             {:name "left-shoulder" :size 3}
                             {:name "left-upper-arm" :size 3}
                             {:name "chest" :size 10}
                             {:name "back" :size 10}
                             {:name "left-forearm" :size 3}
                             {:name "abdomen" :size 6}
                             {:name "left-kidney" :size 1}
                             {:name "left-hand" :size 2}
                             {:name "left-knee" :size 2}
                             {:name "left-thigh" :size 4}
                             {:name "left-lower-leg" :size 3}
                             {:name "left-achilles" :size 1}
                             {:name "left-foot" :size 2}])


(defn matching-part
  [part]
  {:name (clojure.string/replace (:name part) #"^left-" "right-")
   :size (:size part)})

(matching-part {:name "left-ear" :size 1})

(defn symmetrize-body-parts
  "Expects a seq of maps that have a :name and :size"
  [asym-body-parts]
  (loop [remaining-asym-parts asym-body-parts 
         final-body-parts []]
    (if (empty? remaining-asym-parts) 
      final-body-parts
      (let [[part & remaining] remaining-asym-parts] 
        (recur remaining 
               (into final-body-parts
                     (set [part (matching-part part)])))))))


(symmetrize-body-parts asym-hobbit-body-parts)




(defn function [[head & rest]]
  (if (= head nil) "la lista è vuota"
      "la lista non è vuota"))

(function (list 1 2 3 4))

(def a (list 2 3 4 5))

(cons 6 a)

(concat a (list 6))


(defn reverse-recursive [xs]
  (if (empty? xs) (list)
      (concat (reverse-recursive (rest xs)) (list (first xs)))))

(defn reverse-recursive2 [[x & tail]]
  (if (empty? tail) (list x)
      (concat (reverse-recursive2 tail) (list x))))

(reverse-recursive2 a)

(defn reverse-loop1 [xs]
  (loop [result (list ) dec xs ]
    (if (empty? dec)
      result
      (recur (cons (first dec) result) (rest dec)))))

(reverse3 a)

(defn reverse-loop2 [xs]
  (loop [dec xs result (list )]
    (if (empty? dec)
      result
      (recur (rest dec) (cons (first dec) result)))))

(reverse4 a)

(defn reverse-reduce [xs]
  (reduce (fn [resulting-list el]
            (cons el resulting-list))
          (list ) xs))

(reverse-reduce a)

(eval (list 'def 'a '(+ 1 2)))

(read-string "(+ 1 2)")

(type (eval (read-string "#(+ 1 %)")))

(defmacro my-print
  [expression]
  (list 'let ['result expression]
        (list 'println 'result)
        'result))


(def a {:google "value"})
(def b {:bing "value"})

(defn mock-api-call
  [result]
  (Thread/sleep (rand-int 10000))
  result)

(let [winner (promise)]
  (doseq [website [a b]]
    (future (deliver winner (mock-api-call website))))
   @winner)

(def fred (atom {:cuddle-hunger-level 0
                 :percent-deteriorated 0}))


(defn update [fred]
  (swap! fred
         (fn [current-state]
           (merge-with + current-state {:cuddle-hunger-level 1}))))


(defn increase-cuddle-hunger-level
  [zombie-state increase-by]
  (merge-with + zombie-state {:cuddle-hunger-level increase-by}))


(increase-cuddle-hunger-level @fred 10)

(deref fred)


;; Posso tenere traccia degli stati precedenti di un atom

(let [num (atom 1)
      s1 @num]
  (swap! num inc)
  s1
  @num)

;; Refs

(def sock-varieties
  #{"darned" "argyle" "wool" "horsehair" "mulleted"
    "passive-aggressive" "striped" "polka-dotted"
    "athletic" "business" "power" "invisible" "gollumed"})

(defn sock-count
  [sock-variety count]
  {:variety sock-variety
   :count count})

(defn generate-sock-gnome
  "Create an initial sock gnome state with no socks"
  [name]
  {:name name
   :socks #{}})

(def sock-gnome (ref (generate-sock-gnome "Barumpharumph")))
(def dryer (ref {:name "LG 1337"
                 :socks (set (map #(sock-count % 2) sock-varieties))}))

(defn steal-sock
  [gnome dryer]
  (dosync
   (when-let [pair (some #(if (= (:count %) 2) %) (:socks @dryer))]
     (let [updated-count (sock-count (:variety pair) 1)]
       (alter gnome update-in [:socks] conj updated-count)
       (alter dryer update-in [:socks] disj pair)
       (alter dryer update-in [:socks] conj updated-count)))))

(steal-sock sock-gnome dryer)
(steal-sock sock-gnome dryer)

(:socks @sock-gnome)

(:socks @dryer)

(def alphabet-length 26)

;; Vector of chars, A-Z
(def letters (mapv (comp str char (partial + 65)) (range alphabet-length)))

(defn random-string
  "Returns a random string of specified length"
  [length]
  (apply str (take length (repeatedly #(rand-nth letters)))))
  
(defn random-string-list
  [list-length string-length]
  (doall (take list-length (repeatedly (partial random-string string-length)))))

(def orc-names (random-string-list 3000 7000))

(time (dorun (pmap clojure.string/lower-case orc-names)))

(def numbers [1 2 3 4 5 6 7 8 9 10])

(map (fn [number-group] (doall (map inc number-group)))
      (partition-all 3 numbers))

(defn dec [i]
  (if (= i 0)
    5
    (do (dec (- i 1))
        (println "Ciao"))))

(dec 10)


(def counter (ref 0))

(defn produce [] (dosync (alter counter inc)))

(defn consume [] (dosync (alter counter dec)))

(defn producer-loop [i]
  (if (= i 0)
    "finito"
    (do (produce )
        (producer-loop (- i 1)))))

(producer-loop 10)

(deref counter)

(defn consumer-loop [i]
  (if (= i 0)
    "finito"
    (do (consume )
        (consumer-loop (- i 1)))))

(future (consumer-loop 9))
(future (producer-loop 10))

(deref counter)



