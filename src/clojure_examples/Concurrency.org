#+TITLE: Note sulla Concorrenza e la Mutevolezza in Clojure
#+AUTHOR: Eros Martinelli

* Abstract
  Queste note contengono degli appunti _on the fly_ sulla concorrenza e i cambiamenti di stato in clojure. In pratica queste note servono
  come breve riepilogo di alcuni costrutti utilizzati nella programmazione concorrente (si dice cosi'?) in Clojure.

* Concorrenza
Qui ci starebbe una bella nota sui thread e sulla concorrenza in generale. 

** Futures
Utilizzando i _futures_ posso definire una procedura e farla eseguire su un thread diverso da quello in cui sono, senza chiedere che essa
venga eseguita immediatamente ma con la possibilità che in futuro (da qui il nome) io chieda il risultato di tale procedura. Supponiamo che
il nostro thread principale (quello in cui ci troviamo) sia composto da due funzioni/procedure distinte e indipendenti tra loro P1 e P2.
Quando nel nostro thread valutiamo la procedura P1, fintanto che essa non sarà conclusa, il thread non procederà alla valutazione di P2.
Qualora P1 sia computazionalmente "pesante" e richieda una quantità non indifferente di tempo per essere completata (un esempio potrebbe essere
una chiamata ad una API di qualche sito), questo potrebbe portare a problemi di prestazione. In questo caso conviene utilizzare un future
per P1, in modo che il thread principale possa valutare P2 senza dover aspettare che P1 sia concluso.

Per ottenere il valore di un future si usa 'deref' o '@'. Nota che nel caso in cui il future non abbia ancora ottenuto un valore, chiamando
deref il thread corrente si blocca fintanto che la procedura associata al future non sia stata completamente valutata. Esiste anche l'opportunità di
assegnare un valore al future nel caso la procedura non venga valutata in un lasso di tempo specificato dall'utente.

Come esempio consideriamo S1 ed S2 definiti in questo modo:
#+begin_src clojure
(def s1 (future (Thread/sleep 1000 10)))
(def s2 (+ 1 2))
s2
@s1
#+end_src
Il risultato di s1 non influenza s2 ma ne rallenterebbe l'eseguzione nel caso volessi valutarlo prima di s2.

Un altro caso in cui i futures sono utili è nel parallelizzare delle pipeline di funzioni, come spiegato qui: https://levelup.gitconnected.com/back-to-the-future-in-clojure-934b85a3d08e

** Delays 
I _delays_ sono simili ai futures, con la differenza che la procedura a cui si riferiscono viene valutata quando vengono dereferenziati
utilizzando 'force'. Per questo motivo è comodo utilizzare i delays quando ci sono procedure i cui risultati non necessariamente verranno
utilizzati nel nostro programma e che quindi è opportuno valutare solo in caso di bisogno. Per esempio, se avessi una lista contenente dei
delays e volessi traversarla valutando ogni elemento, utilizzando un delay il procedimento sarebbe molto piü lento che utilizzando dei futures;
in quanto la valuzatione di un futures non parte appena viene dereferenziato come nel caso di un delay.

Supponiamo di avere una lista di documenti d_1,...,d_n da inviare ad un indirizzo mail. Supponiamo di voler inviare un messaggio al 
mittente per avvisarlo quale sia il primo tra gli d_j ad essere stato inviato. Utilizzando un deley possiamo far si che il mittente riceva
solo un messaggio e non n (uno per ciascuno dei documenti); questo è possibile perchè un delay (come un future) viene valutato solo una volta
e ogni volta che lo dereferenziamo otteniamo il valore in memoria risultante dalla prima valutazione.
Ecco un esempio di una possibile implementazione:
#+begin_src clojure
(def documents ["d_1" "d_2" "d_3"])
(defn email-user
  [email-address]
  (println "Sending notification to" email-address))

(defn upload-document
  "Needs to be implemented"
  [headshot]
  (Thread/sleep (rand-int 10000))
  headshot)

(let [notify (delay (email-user "prova@gmail.com"))]
  (doseq [d documents]
    (future (upload-document d)
            (force notify))))
#+end_src

** Promises
Le promesse servono per dichiarare dei risultati previsti ma che ancora non si hanno. Per esempio, supponiamo di avere delle procedure
p_1,...,p_n che vengono eseguite parallelalmente. Ancora non possiamo sapere quale tra le p_j finisca per prima di essere valutata; possimao
definire una variabile, inizializzata come promessa, che conterrà il nome della prima procedura ad essere valutata.

Per esempio, supponiamo di voler comparare la velocità tra due (o piü) motori di ricerca e di voler vedere quale sia il piü veloce. Usando
una promise possiamo procedere a questo modo
#+begin_src clojure
(def a {:google "value"})
(def b {:bing "value"})

(defn mock-api-call
  [result]
  (Thread/sleep (rand-int 10000))
  result)

(let [winner (promise)]
  (doseq [website [a b]]
    (future (deliver winner (mock-api-call website))))
   @winner)
#+end_src

* Mutevolezza
Per maggiori informazioni sul concetto di stato, leggi: https://clojure.org/about/state.

Le strutture dati in Clojure - di norma - sono immutabili; questo significa che quando - per esempio - chiamiamo una funzione che effettui
delle modifiche agli elelementi di una lista, quello che otteniamo è una nuova lista con le modifiche effettuate, non la vecchia lista modificata.
A volte perö, è conveniente avere la possibilità di mutare lo stato di una variabile. Clojure ci permette di fare ciö mantenendo l'immutabilità
di fondo.

In Clojure abbiamo tre possibilità per dichiarare variabili il cui stato puo' mutare nel corso del tempo:
- Atom;
- Ref;
- Agent.
  
Ciascuna di esse è stata progettata per degli usi specifici, ma tutte e tre si basano sul concetto di stato. In Clojure, una variabile mutabile
viene rappresentata come un puntatore ad una variabile (non mutabile). Quando cambiamo lo stato di questa variabile, ciö che facciamo è di
modificare ciö a cui si riferisce la variabile, non di modificare il valore a cui essa punta; in sostanza è il puntatore che cambia, ciö che
viene puntato cambia si', ma solo perchè cessa di essere puntato dalla variabile mutabile e non perchè il suo valore cambi. La differenza
tra questo approccio e quello "classico" della programmazione ad oggetti e che il puntatore punta ad una variabile e non ad uno spazio di
memoria. 

Invece di dire che è l'informazione sullo stato è cambiata, possiamo dire che abbiamo ottenuto una nuova informazione e che lo stato viene
aggiornato tenendo conto di questa nuova informazione. 

** Atoms
Gli atomi sono utili quando dobbiamo modellizzare lo stato di entità indipendenti (da cui il nome); entità il cui cambiamento di stato non
è influenzato da un'altra operazione. Gli atoms sono sincroni (inserire descrizione)...
#+begin_src clojure
(def current-track (atom {:title "Credo" :composer "Byrd"}))

(reset! current-track {:title "Spem in Alium" :composer "Tallis"})

(swap! current-track assoc :title "Sancte Deus") ;; Per aggiornare solo un elemento di una collezione.
#+end_src
Un loro tipico utilizzo è per le memoization
#+begin_src clojure
(defn memoize [f]
  (let [mem (atom {})]
    (fn [& args]
      (if-let [e (find @mem args)]
        (val e)
        (let [ret (apply f args)]
          (swap! mem assoc args ret)
          ret)))))
#+end_src
Nella definizione di un atom posso anche includere un validator, cioè una condizione che una transazione deve soddisfare affinchè essa
effettivamente avvenga. Per esempio, posso richiedere che un atom sia un numero e se dovessi modificarlo inserendo un altro tipo di dato
la transazione non avverrebbe.
#+begin_src clojure
(def counter (atom 0 :validator number?)
#+end_src
** Refs
I refs vengono utilizzati quando è necessario orchestrare dei cambiamenti coordinati, cioè quando dobbiamo effettuare piü cambiamenti di
stato simultaneamente. Un esempio è dato da qualsiasi transazione che involva due persone p_1 (che riceve i soldi) e p_2; in questo caso
il denaro posseduto da p_1 aumenta nello stesso momento in cui quello posseduto da p_2 diminuisce.

Per effettuare una o piü modifiche coordinate ad un insieme di refs viene utilizzato il costrutto 'dosync'. Come per gli atoms posso
utilizzare dei validators
#+begin_src clojure
(defrecord Message [sender text])

(->Message "Aaron" "Hello")

(def messages (ref ()))

(defn add-message [msg]
  (dosync (alter messages conj msg)))

(add-message (->Message "user 2" "howdy"))

(add-message (->Message "user 1" "hello"))

(defn valid-message? [msg]
  (and (:sender msg) (:text msg)))

(def validate-message-list #(every? valid-message? %))

(def messages (ref () :validator validate-message-list))

(add-message "not a valid message")
#+end_src

Per alterare uno stato possiamo usare 'alter' e 'commute'. Di norma il primo è da preferirsi al secondo, in quanto piü sicuro; il secondo
è utile quando si hanno delle modifiche da apportare il cui ordine non è importante (da cui il nome).
Il comportamento di 'alter' è dato da:
- Leggere lo stato corrente al di fuori della transazione;
- Comparare il valore precedente con il valore della ref all'interno della transazione;
- Eseguire la transazione se i due valori coincidono, altrimenti riprovare.

Mentre 'commute' salta il controllo dell'ultimo punto e altera il valore ottenuto nel primo, migliorando le performance locali della
transazione. 

** Vars
Le vars regolano le associazioni tra simboli e oggetti. Di norma, come la maggior parte dei costrutti, sono immutabili, perö c'è la
possibilità di modificare il loro valore utilizzando un "dynamic binding".
La sintassi per definire un dynamic binding è:
#+begin_src clojure
(def ^:dynamic *indirizzo* "nome.cognome@gmail.com")
#+end_src
mentre per cambiarne lo stato si utilizza la binding macro
#+begin_src clojure
(binding [*indirizzo* "nuovo.indirizzo@gmail.com"]
  *indirizzo*)
#+end_src
Il loro indirizzo principale è di essere utilizzate per identificare risorse che una o piü funzioni utilizzano, senza che queste funzioni
debbano averle come parametro. In pratica, utilizzando una dynamic var e un binding all'interno del corpo della funzione, ho la possibilità
di snellire la segnatura della funzione.

Supponiamo di voler mandare una notifica all'indirizzo email "indirizzo". Un modo per procedere è il seguente
#+begin_src clojure
(defn notify
  [message]
  (str "TO: " *indirizzo* "\n"
       "MESSAGE: " message))
#+end_src
Se volessimo cambiare l'indirizzo, invece che scrivere una nuova funzione che abbia come parametro un indirizzo, possiamo usare il binding:
#+begin_src clojure
(binding [*indirizzo* "test@gmail.com"]
  (notify "test!"))
#+end_src
Un altro uso che si puo' fare delle vars è un uso duale. Nel caso precedente abbiamo usato le vars come argomento "fantasma" per una funzione;
dualmente, possiamo utilizzare una var come risultato "virtuale".

Consideriamo il seguente programma:
#+begin_src clojure
(def ^:dynamic *oracle-answer* nil)

(defn oracle-riddle [your-answer]
  (let [answer (rand-int 10)]
    (when (thread-bound? #'*oracle-answer*)
      (set! *oracle-answer* answer))
    (if (= your-answer answer)
      "Your answer is correct!"
      "Your answer is not corrrect!")))

(binding [*oracle-answer* nil]
  (println (oracle-riddle 2))
  (println "The correct answer was:" *oracle-answer*))
#+end_src
Abbiamo un oracolo che "pensa"un numero da 0 a 9 e noi che dobbiamo tentare di indovinarlo. Utilizzando il dynamic binding possiamo "recuperare"
il valore pensato dall'oracolo senza bisogno che esso sia passato come risultato della funzione 'oracle-answer'. Nota come sia necessario
eseguire un binding per poter accedere al valore di *oracle-answer*.



